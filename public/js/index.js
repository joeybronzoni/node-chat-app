
//Leftoff- SECTION-9 LECTURE-114

console.log('index is and running:');
const socket = io();

socket.on('connect', function  () { //<--arrow function s wont work in some phones
  console.log('Connected to the server');
});

socket.on('disconnect', function () {
  console.log('Disconnected from server');
});



// // Using jQuery
/*
socket.on('newMessage',function (message) {
  console.log('New Message', message);
  const li = $('<li></li>');
  li.text(`${message.from}: ${message.text}`);
  $('#messages').append(li);
});
*/

// Using vanilla JS
socket.on('newMessage',function (message) {
  console.log('New Message', message);

  const ol = document.getElementById('messages');
  const li = document.createElement('li');
  li.textContent = `${message.from}: ${message.text}`;
  ol.appendChild(li);

});


// Using jQuery
/*
$('#message-form').on('submit', function (e) {
  e.preventDefault();

  socket.emit('createMessage', {
	from: 'User',
	text: $('[name=message]').val()
  }, function () {

  });
});
*/

// Using vanilla
const messageForm = document.getElementById('message-form');

messageForm.addEventListener('submit', function (e) {
  e.preventDefault();
  console.log('Submitted');
  const messageInput = document.querySelector('input[name=message]').value;

  emitMessage(messageInput);
});

function emitMessage (messageInput) {
  socket.emit('createMessage', {
	from: 'User',
	text: messageInput
  }, function () {

  });
}
