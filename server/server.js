// remote
const path = require('path');
const http = require('http');
const express = require('express');
const fs = require('fs');
const socketIO = require('socket.io');

//local
const { generateMessage } = require('./utils/message');
const publicPath = path.join(__dirname, '../public');
const PORT = process.env.PORT || 3030;
const app = express();
const server = http.createServer(app);
const io = socketIO(server);

//MiddleWares
app.use(express.static(publicPath));

app.use((req, res, next) => {
  const now = new Date().toString();
  const log = `${now}: ${req.method} ${req.url}`;

  console.log(log);
  fs.appendFile('server.log', `${log} \n`, (err) => {
	err ? console.log('Unable to append to log file') : 'Log Success';
  });
  next();
});

// main socket
io.on('connection', (socket) => { //<--socket represents the individual socket as aposed to all the users connected to the server
  console.log('New user connected');

  // emit custom event (emits an event to a single connection as aposed to io.emit())
  socket.emit('newMessage', generateMessage('Admin', 'Welcome to the chat app'));

  // emit an event to everybody except one specific user
  socket.broadcast.emit('newMessage', generateMessage('Admin', 'New User Joined'));

  // Standard event listener
  socket.on('createMessage', (message, callback) => {
	console.log('createMessage is:', message);
	// emits an event to every single connection (as aposed to socket.emit())
	io.emit('newMessage', generateMessage(message.from, message.text));
	callback('This is from the server');
  });

  // not for messaging, lets user know if disconnects from server
  socket.on('disconnect', () => {
	console.log('User was Disconnected');
  });

});

server.listen(PORT, () => {
  console.log(`Express running on PORT, ${PORT}`);
});
