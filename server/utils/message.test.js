const expect = require('expect');


const { generateMessage } = require('./message');


describe('generateMessage', () => {
  it('does generate the correct message object', () => {
	const from = 'Joey';
	const text = 'Some message';
	const message = generateMessage(from, text);

	expect(message.createdAt).toBeA('number');
	expect(message).toInclude({from, text});
  });

});
